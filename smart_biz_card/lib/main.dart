import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'login.dart';
import 'register.dart';
void main() => runApp(SmartBizCard());

class SmartBizCard extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Smart Biz Card',
      theme: ThemeData(
        primarySwatch: Colors.teal,
      ),
      home: myHomePage(),
    );
  }
}
class myHomePage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Smart Biz Card'),
        actions: <Widget>[
          IconButton(icon: new Icon(Icons.person_add), onPressed: (){}, color: Colors.white,),
        ],
      ),
      body: new Container(
        foregroundDecoration: new BoxDecoration(
          color: Colors.lightBlue.withOpacity(0.6),
        ),
        decoration: new BoxDecoration(
          image: new DecorationImage(image: new AssetImage('images/sbc_wallpaper.jpg'),fit: BoxFit.fill,),
        ),

        child: Login(),
      ),
    );
  }

}